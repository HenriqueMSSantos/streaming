class CreateSequences < ActiveRecord::Migration[5.2]
  def change
    create_table :sequences do |t|
      t.integer :numero
      t.string :nome
      t.timestamps
    end
  end
end
