class ContentsUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :contents_users do |t|
      t.references :content, index: true
      t.references :user, index: true

      t.timestamps
    end

    add_foreign_key :contents_users, :contents, on_update: :cascade, on_delete: :restrict
    add_foreign_key :contents_users, :users, on_update: :cascade, on_delete: :restrict
  end
end
