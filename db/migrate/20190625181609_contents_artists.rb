class ContentsArtists < ActiveRecord::Migration[5.2]
  def change
    create_table :contents_artists do |t|
      t.string :cargo
      t.references :content, index: true
      t.references :artist, index: true

      t.timestamps
    end
    add_foreign_key :contents_artists, :contents, on_update: :cascade, on_delete: :restrict
    add_foreign_key :contents_artists, :artists, on_update: :cascade, on_delete: :restrict

  end
end
