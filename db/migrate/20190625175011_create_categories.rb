class CreateCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :categories do |t|
      t.string :genero
      t.string :estilo

      t.timestamps
    end
  end
end
