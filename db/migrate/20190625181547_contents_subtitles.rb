class ContentsSubtitles < ActiveRecord::Migration[5.2]
  def change
    create_table :contents_subtitles do |t|
      t.references :content, index: true
      t.references :subtitle, index: true

      t.timestamps
    end

    add_foreign_key :contents_subtitles, :contents, on_update: :cascade, on_delete: :restrict
    add_foreign_key :contents_subtitles, :subtitles, on_update: :cascade, on_delete: :restrict
  end
end
