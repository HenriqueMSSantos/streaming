class ContentsPrizes < ActiveRecord::Migration[5.2]
  def change
    create_table :contents_prizes do |t|
      t.boolean :venceu
      t.references :content
      t.references :prize

      t.timestamps
    end

    add_foreign_key :contents_prizes, :contents, on_update: :cascade, on_delete: :restrict
    add_foreign_key :contents_prizes, :prizes, on_update: :cascade, on_delete: :restrict
  end
end
