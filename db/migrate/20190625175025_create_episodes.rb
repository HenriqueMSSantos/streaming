class CreateEpisodes < ActiveRecord::Migration[5.2]
  def change
    create_table :episodes do |t|
      t.string :sinopse
      t.string :nome
      t.string :duracao
      t.string :temporada
      t.references :show, index: true

      t.timestamps
    end
    add_foreign_key :episodes, :shows, on_update: :cascade, on_delete: :restrict

  end
end
