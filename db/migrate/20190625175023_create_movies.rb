class CreateMovies < ActiveRecord::Migration[5.2]
  def change
    create_table :movies do |t|
      t.string :duracao
      t.references :content, index: true
      t.references :sequence, index: true

      t.timestamps
    end
    add_foreign_key :movies, :contents, on_update: :cascade, on_delete: :restrict
    add_foreign_key :movies, :sequences, on_update: :cascade, on_delete: :restrict
  end
end
