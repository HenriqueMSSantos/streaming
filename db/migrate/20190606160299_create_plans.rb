class CreatePlans < ActiveRecord::Migration[5.2]
  def change
    create_table :plans do |t|
      t.string :nome
      t.float :valor
      t.integer :telas
      t.string :qualidade

      t.timestamps
    end
  end
end
