class CreateCategoria < ActiveRecord::Migration[5.2]
  def change
    create_table :categoria do |t|
      t.string :estilo
      t.string :genero
      t.references :user, index: true

      t.timestamps
    end
  end
end
