class CreateShows < ActiveRecord::Migration[5.2]
  def change
    create_table :shows do |t|
      t.integer :temporadas
      t.references :content, index: true

      t.timestamps
    end
    add_foreign_key :shows, :contents, on_update: :cascade, on_delete: :restrict
  end
end
