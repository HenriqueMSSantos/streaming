class CreateContents < ActiveRecord::Migration[5.2]
  def change
    create_table :contents do |t|
      t.string :nome
      t.datetime :data
      t.text :sinopse
      t.references :classification, index: true
      t.references :category, index: true
      t.references :producer, index: true

      t.timestamps
    end
    add_foreign_key :contents, :classifications, on_update: :cascade, on_delete: :restrict
    add_foreign_key :contents, :categories, on_update: :cascade, on_delete: :restrict
    add_foreign_key :contents, :producers, on_update: :cascade, on_delete: :restrict
  end
end
