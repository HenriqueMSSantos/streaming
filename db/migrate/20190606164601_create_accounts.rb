class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.string :nome
      t.string :identidade
      t.integer :idade
      t.references :user, references: :users

      t.timestamps
    end
    add_foreign_key :accounts, :users, on_update: :cascade, on_delete: :cascade
  end
end
