class ContentsLanguages < ActiveRecord::Migration[5.2]
  def change
    create_table :contents_languages do |t|
      t.references :content, index: true
      t.references :language, index: true

      t.timestamps
    end

    add_foreign_key :contents_languages, :contents, on_update: :cascade, on_delete: :restrict
    add_foreign_key :contents_languages, :languages, on_update: :cascade, on_delete: :restrict
  end
end
