class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?

  def accounts
    @accounts = AccountsRepository.current_user(current_user.id)
  end
  
  protected
  
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up,
      keys: [:first_name, :last_name, :email, :password, :password_confirmation, :plan_id,
      address_attributes: [:zipcode, :street, :neighborhood, :city, :state]])
    devise_parameter_sanitizer.permit(:account_update,
      keys: [:first_name, :last_name, :email, :password, :password_confirmation,
        address_attributes: [:zipcode, :street, :neighborhood, :city, :state]])
  end

  def after_sign_in_path_for(resource)
    select_account_index_path
  end
end
