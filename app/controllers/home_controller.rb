class HomeController < ApplicationController
  before_action :already_logged
  
  def index
  end

  protected 

  def already_logged
    respond_to do |format|
      if user_signed_in?
        format.html { redirect_to select_account_index_path, notice: 'Você está logado.' }
      else
        format.html { render :index }
      end
    end
  end
end
