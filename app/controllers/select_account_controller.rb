class SelectAccountController < ApplicationController
  before_action :authenticate_user!
  before_action :accounts
  before_action :verify_account

  def index
    accounts 
  end

  protected

  def verify_account
    respond_to do |format|
      if current_user.plan_id == nil
        format.html { redirect_to plans_path, notice: "Olá #{current_user.full_name} ;), escolha seu plano" } 
      elsif @accounts.count == 0
        format.html { redirect_to new_account_path, notice: "Olá #{current_user.full_name} ;), registre sua conta." } 
      elsif @accounts.count == 1
        format.html { redirect_to dashboard_index_path, notice: "Bem vindo de novo!" } 
      else
        format.html { render :index, notice: "Selecione sua conta!" }
      end
    end
  end
end
