class DashboardController < ApplicationController
  before_action :authenticate_user!
  before_action :accounts
  layout 'inside'

  def index
    accounts
    @movies = Movie.all
    @shows = Show.all
  end  
end
