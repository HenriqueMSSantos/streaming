class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  
  belongs_to :plan, optional: true
  accepts_nested_attributes_for :plan, allow_destroy: true
  belongs_to :address, optional: true
  accepts_nested_attributes_for :address, allow_destroy: true
  has_many :accounts

  def full_name
    "#{first_name} #{last_name}"
  end
end
