json.extract! plan, :id, :valor, :telas, :qualidade, :created_at, :updated_at
json.url plan_url(plan, format: :json)
