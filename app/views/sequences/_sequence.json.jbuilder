json.extract! sequence, :id, :created_at, :updated_at
json.url sequence_url(sequence, format: :json)
