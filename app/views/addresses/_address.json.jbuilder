json.extract! address, :id, :cidade, :pais, :rua, :numero, :created_at, :updated_at
json.url address_url(address, format: :json)
