class AccountDecorator < Draper::Decorator
    
    def full_name
		object.user.full_name
	end
end