class AccountsOperation

    def self.all
        AccountsRepository.all
    end

    def self.current_user
        AccountsRepository.current_user
    end
end
