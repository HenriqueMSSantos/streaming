class AccountsRepository 

	def	self.all
		AccountDecorator.decorate_collection(Account.all)
	end

	def self.current_user(id)
		Account.where(user_id: id)
	end
end
