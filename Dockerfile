# A simple Dockerfile for a RoR application

FROM ruby:2.5.5

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /streaming
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - && apt-get install -y nodejs

WORKDIR /streaming

ADD Gemfile /streaming/Gemfile
ADD Gemfile.lock /streaming/Gemfile.lock

RUN bundle install
ADD . /streaming