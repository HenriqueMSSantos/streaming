Rails.application.routes.draw do
  resources :prizes
  resources :categories
  resources :episodes
  resources :sequences
  resources :shows
  resources :movies
  resources :subtitles
  resources :languages
  resources :classifications
  resources :contents
  resources :artists
  resources :plans
  resources :addresses
  resources :accounts
  resources :categoria
  resources :dashboard
  resources :select_account
  root 'home#index'
  devise_for :users, controllers: { sessions: "users/sessions", registrations: "users/registrations", passwords: "users/passwords",confirmations: "users/confirmations" }
  as :user do 
    get '/users/sign_out' => 'devise/sessions#destroy'
  end
  get 'home/index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
